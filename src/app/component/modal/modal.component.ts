import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() isOpen: boolean = false;
  @Output() closeEvent = new EventEmitter<void>();
  modalContent: any;

  ngOnInit(): void {
    this.modalContent = document.querySelector('.modal-content');
  }

  onModalOutsideClick(event: MouseEvent) {
    if (!this.modalContent?.contains(event.target as Node)) {
      this.close();
    }
  }

  onModalContentClick(event: MouseEvent) {
    event.stopPropagation();
  }

  close() {
    document.querySelector('.modal-content')?.classList.add('hidden');
    setTimeout(() => {
      this.closeEvent.emit();
    }, 500);
  }
}
