import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit {
  selectedLang = 'en';

  constructor(private translate: TranslateService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.route.params.subscribe(({ language }) => {
      if (language === 'vi' || language === 'en') {
        this.translate.setDefaultLang(language);
        this.selectedLang = language;
      } else {
        this.router.navigate(['home', 'en']);
        this.selectedLang = 'en';
      }
    });
  }

  isOpenModal = false;

  toggleChangeLang() {
    if (this.selectedLang === 'vi') {
      this.selectedLang = 'en';
      this.translate.setDefaultLang('en');
    } else {
      this.selectedLang = 'vi';
      this.translate.setDefaultLang('vi');
    }
  }

  onClickSendRequest() {
    this.isOpenModal = true;
  }

  onClickCloseModal() {
    this.isOpenModal = false;
  }
}
