import { Component, ElementRef, HostListener, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnChanges, OnInit {
  @Input() title = '';
  @Input() listItem: any;
  @ViewChild('chart') chartComponent?: ElementRef;
  @HostListener('scroll', ['$event']) onScroll(event: Event) {
    const scrollHeigh = (event.target as HTMLElement).scrollTop;
    if (this.heighItem) {
      const selectItemIdx = Math.ceil(scrollHeigh / (this.heighItem + 30));
      this.showingItem = this.listItem[selectItemIdx];
    }
  }

  showingItem: any;
  heighItem?: number = 0;

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.heighItem = document.querySelector('.chart-item')?.clientHeight;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.listItem) {
      this.showingItem = this.listItem[0];
    }
  }

  onSelect(item: any) {
    this.showingItem = item;
  }
}
