import { Component, OnInit } from '@angular/core';
import { bannerImgs } from 'src/utils/sample-data';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
})
export class BannerComponent implements OnInit {
  imgs: any;
  dots: any;
  bannerImgs = bannerImgs;
  timer: any;
  currentImg = 0;
  prevImg = 0;
  interval = 3000;

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.imgs = document.querySelectorAll('.banner-img');
    this.dots = document.querySelectorAll('.banner-dot');
    this.setTimer();
  }

  setTimer() {
    this.timer = setTimeout(() => {
      this.prevImg = this.currentImg;
      this.currentImg += 1;
      if (this.currentImg >= this.imgs.length) {
        this.currentImg = 0;
      }
      this.changeSlide();
    }, this.interval);
  }

  changeSlide(n?: number) {
    if (n != undefined) {
      this.prevImg = this.currentImg;
      this.currentImg = n;
    }
    (this.imgs[this.prevImg] as HTMLElement).style.opacity = '0';
    (this.dots[this.prevImg] as HTMLElement).classList.remove('active');
    (this.imgs[this.currentImg] as HTMLElement).style.opacity = '1';
    (this.dots[this.currentImg] as HTMLElement).classList.add('active');
    clearTimeout(this.timer);
    this.setTimer();
  }
}
