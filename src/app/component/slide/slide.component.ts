import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss'],
})
export class SlideComponent implements OnChanges {
  @Input() numberSlideShow: number = 0;
  @Input() slideItems: any;
  @Input() slideTitle: string = '';
  slideComponent: any; //Div cha chứa slide
  listItem: any[] = []; // list các slide của component
  listItemDiv: any;
  slideWidth: number | undefined; // chiều dài của các slide show
  slideItemWidth: number | undefined; // chiều dài của 1 item
  marginWidth = 0; // chiều dài đã margin
  hasNext: any; // số phần tử còn lại có thể next
  hasPrev: any; // số phần tử còn lại có thể prev

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.slideItems) {
      this.slideItems?.forEach((artistItem: any) => {
        this.slideComponent = artistItem.nativeElement;
      });

      this.listItem = this.slideComponent.querySelectorAll('.slide-item');
      this.listItemDiv = this.slideComponent.querySelector('.list-item');
      this.slideWidth = this.slideComponent.querySelector('.sliders')?.clientWidth;
      this.hasNext = this.listItem.length - this.numberSlideShow;
      this.hasPrev = 0;
      if (this.slideWidth) {
        // set chiều dài của 1 item
        this.slideItemWidth = (this.slideWidth - 10 * this.numberSlideShow) / this.numberSlideShow;

        for (let i = 0; i < this.listItem?.length; i++) {
          (this.listItem[i] as HTMLElement).style.minWidth = `${this.slideItemWidth}px`;
        }
      }
    }
  }

  onClickNext() {
    if (this.hasNext && this.hasNext >= this.numberSlideShow && this.slideWidth) {
      // khi số slide có thể next còn nhiều hơn số slide cần show
      (this.listItemDiv as HTMLElement).style.marginLeft = `-${this.marginWidth + this.slideWidth}px`;
      this.marginWidth += this.slideWidth;
      this.hasNext -= this.numberSlideShow;
      this.hasPrev += this.numberSlideShow;
    } else if (this.hasNext && this.hasNext < this.numberSlideShow && this.slideItemWidth) {
      // khi số slide có thể next còn ít hơn số slide cần show
      (this.listItemDiv as HTMLElement).style.marginLeft = `-${this.marginWidth + this.hasNext * (this.slideItemWidth + 10)}px`;
      this.marginWidth += this.hasNext * (this.slideItemWidth + 10);
      this.hasNext = 0;
      this.hasPrev = this.listItem.length - this.numberSlideShow;
    }
  }

  onClickPrev() {
    if (this.hasPrev && this.hasPrev >= this.numberSlideShow && this.slideWidth) {
      // khi số slide có thể prev còn nhiều hơn số slide cần show
      (this.listItemDiv as HTMLElement).style.marginLeft = `-${this.marginWidth - this.slideWidth}px`;
      this.marginWidth -= this.slideWidth;
      this.hasNext += this.numberSlideShow;
      this.hasPrev -= this.numberSlideShow;
    } else if (this.hasPrev && this.hasPrev < this.numberSlideShow && this.slideItemWidth) {
      // khi số slide có thể prev còn ít hơn số slide cần show
      (this.listItemDiv as HTMLElement).style.marginLeft = `0px`;
      this.marginWidth = 0;
      this.hasPrev = 0;
      this.hasNext = this.listItem.length - this.numberSlideShow;
    }
  }
}
