import { HttpClient, HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule, TransferState } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BannerComponent } from './component/banner/banner.component';
import { ChartComponent } from './component/chart/chart.component';
import { ModalComponent } from './component/modal/modal.component';
import { NavBarComponent } from './component/nav-bar/nav-bar.component';
import { SideBarComponent } from './component/side-bar/side-bar.component';
import { SlideComponent } from './component/slide/slide.component';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './pages/home/home.component';

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}

export function appInitializerFactory(translate: TranslateService) {
  return () => {
    translate.setDefaultLang('en');
    translate.use('en');
    return translate.getTranslation('en').toPromise();
  };
}

@NgModule({
  declarations: [SideBarComponent, NavBarComponent, SlideComponent, BannerComponent, ChartComponent, ModalComponent, AppComponent, HomeComponent],
  imports: [
    CommonModule,
    BrowserModule.withServerTransition({ appId: 'my-app' }),
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
