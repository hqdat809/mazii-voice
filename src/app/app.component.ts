import { ActivatedRoute, Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Component, ElementRef, QueryList, ViewChild, ViewChildren, OnInit } from '@angular/core';
import { albumOutstanding, artists, tags } from 'src/utils/sample-data';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './component/slide/slide.component.scss'],
})
export class AppComponent {}
