import { ActivatedRoute, Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Component, ElementRef, QueryList, ViewChild, ViewChildren, OnInit } from '@angular/core';
import { albumOutstanding, artists, tags } from 'src/utils/sample-data';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  @ViewChild('listArtist') myTemplateRef: any;
  @ViewChildren('artistSlide') artistItems: QueryList<ElementRef> | undefined;
  @ViewChildren('albumSlide') albumItems: QueryList<ElementRef> | undefined;
  @ViewChildren('recommend') recommendItems: QueryList<ElementRef> | undefined;
  @ViewChildren('new') newItems: QueryList<ElementRef> | undefined;
  title = 'voice-mazii';
  artists = artists;
  albums = albumOutstanding;
  voiceChart = albumOutstanding;
  tags = tags;
  language: string = '';

  ngAfterViewInit() {}
}
