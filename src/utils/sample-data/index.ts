export const bannerImgs = [
  'https://images.unsplash.com/photo-1524678606370-a47ad25cb82a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80',
  'https://images.unsplash.com/photo-1470225620780-dba8ba36b745?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
  'https://images.unsplash.com/photo-1487215078519-e21cc028cb29?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
];

export const artists = [
  {
    name: 'Oikawa',
    musicType: 'Pop music',
    avt: 'https://cdn.myanimelist.net/images/characters/6/245977.jpg',
  },
  {
    name: 'Kenma',
    musicType: 'Pop music',
    avt: 'https://cdn.myanimelist.net/images/characters/10/417885.jpg',
  },
  {
    name: 'Kuroo',
    musicType: 'Pop music',
    avt: 'https://cdn.myanimelist.net/images/characters/13/417887.jpg',
  },
  {
    name: 'Tsukishima',
    musicType: 'Pop music',
    avt: 'https://i.pinimg.com/736x/46/5b/c2/465bc2bea375006238bf9f99625311a6.jpg',
  },
  {
    name: 'Tobio',
    musicType: 'Pop music',
    avt: 'https://cdn.myanimelist.net/images/characters/13/417874.jpg',
  },
  {
    name: 'Nishinoya',
    musicType: 'Pop music',
    avt: 'https://api.duniagames.co.id/api/content/upload/file/4872854671579190520.jpg',
  },
];

export const albumOutstanding = [
  {
    id: 1,
    img: 'https://avatar-ex-swe.nixcdn.com/playlist/2023/04/25/b/e/1/e/1682413694625.jpg',
    thumb_img: 'https://vtv1.mediacdn.vn/thumb_w/1000/562122370168008704/2023/4/25/202304240007290-16824639318711102855093.jpg',
    name: 'Down to Earth',
    author: 'Teayang',
    views: 546,
    increase: true,
    decrease: false,
  },
  {
    id: 2,
    img: 'https://avatar-ex-swe.nixcdn.com/playlist/2023/04/20/f/4/0/1/1681979467382.jpg',
    thumb_img: 'https://lastfm.freetls.fastly.net/i/u/500x500/16e39fd39917aa2f93d8ad09530a07fc.jpg',
    name: 'DRIVE',
    author: 'Tiesto',
    views: 546,
    increase: false,
    decrease: false,
  },
  {
    id: 3,
    img: 'https://avatar-ex-swe.nixcdn.com/playlist/2023/04/20/f/4/0/1/1681977073350.jpg',
    thumb_img: 'https://images.genius.com/ae79fb57bf3cdb5a80c01fd0132ae81c.1000x1000x1.jpg',
    name: 'Life Upside Down (EP)',
    author: 'Morgan Evans',
    views: 546,
    increase: false,
    decrease: false,
  },
  {
    id: 4,
    img: 'https://avatar-ex-swe.nixcdn.com/playlist/2022/08/10/8/a/1/e/1660123504328.jpg',
    thumb_img: 'https://avatar-ex-swe.nixcdn.com/playlist/share/2022/08/17/8/f/9/f/1660733437873.jpg',
    name: '22',
    author: 'MONO',
    views: 546,
    increase: false,
    decrease: true,
  },
  {
    id: 5,
    img: 'https://avatar-ex-swe.nixcdn.com/playlist/2022/10/06/4/8/c/9/1665059432710.jpg',
    thumb_img: 'https://avatar-ex-swe.nixcdn.com/playlist/share/2022/10/07/2/8/8/f/1665114985583.jpg',
    name: 'SKY DECADE (EP)',
    author: 'Sơn tùng M-TP',
    views: 546,
    increase: false,
    decrease: true,
  },
  {
    id: 6,
    img: 'https://avatar-ex-swe.nixcdn.com/playlist/2023/04/20/4/c/3/d/1681987165890.jpg',
    thumb_img: 'https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/5/6/3/2/5632d0596d7d6de3d17c74d7442a8080.jpg',
    name: 'RỐC (EP)',
    author: 'Bột Màu Khoai Tây Cà Rốt',
    views: 546,
    increase: false,
    decrease: true,
  },
  {
    id: 7,
    img: 'https://avatar-ex-swe.nixcdn.com/playlist/2022/09/07/8/3/3/7/1662533227963.jpg',
    thumb_img: 'https://bizweb.dktcdn.net/100/411/628/products/vu-bia-f7948db8-d576-4455-9a72-0f80b35d83c3.jpg?v=1660964043207',
    name: 'Một Vạn Năm',
    author: 'Vũ',
    views: 546,
    increase: true,
    decrease: false,
  },
];

export const tags = ['Rock', 'Country', 'Latin', 'R&B', 'Pop', 'Blues/Jazz', 'Rap'];
